/****
 * 请求再次封装
 */

import config from './config.js';

const BASE_URL = config.apiurl;

const request = config_data => {
    config_data.header = {};
    config_data.header = {
            Platform: 'minaapp',
            Authorization: uni.getStorageSync('token'),
        }
        // 默认公参
        // config.data.platform = _platform
    return new Promise((resolve, reject) => {
        // console.log(config, BASE_URL + config.url)
        uni.request({
            method: config_data.method,
            url: BASE_URL + config_data.url,
            data: config_data.data,
            header: config_data.header,
            timeout: 12000,
            success: info => {
                resolve(info.data);
            },
            fail: err => {
                uni.showToast({
                    title: "网络错误",
                    icon: "none"
                });
                reject(err)
            },
        });
    });
};

const post = (url, data, config) => request({
    ...config,
    method: 'POST',
    url,
    data,
});

const get = (url, data, config) => request({
    ...config,
    method: 'GET',
    url,
    data
});

module.exports = {
    request,
    post,
    get
};