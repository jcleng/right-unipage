import Vue from 'vue'
import App from './App'

import store from './store'

Vue.config.productionTip = false

Vue.prototype.$store = store

App.mpType = 'app'
import comLoading from "./components/common/loading.vue";
import comLogin from "./components/common/login.vue";
import comCommon from "./components/common/common.vue";
Vue.component('comLoading', comLoading)
Vue.component('comLogin', comLogin)
Vue.component('comCommon', comCommon)
const app = new Vue({
	store,
	...App
})
app.$mount()
