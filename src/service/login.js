import request from "../common/request.js"
/**
 * 用户登录
 */
export const login = async function(user, pass) {
    // console.log(user, pass);
    const ret = await request.post('/admin/login', {
        username: user,
        password: pass
    })
    if (ret && ret.code == 1) {
        return {
            state: 200,
            data: ret.data,
            msg: ret.msg
        }
    } else {
        return {
            state: -1,
            data: ret.data,
            msg: ret.msg
        };
    }
}