import request from "../common/request.js"
/**
 * 用户订单
 */
export const article_list = async function(page) {
    const ret = await request.get('/article/list/' + page)
    return ret
}