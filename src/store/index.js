import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        // * 只允许 emit 修改
        hasLogin: false,
        // * 只允许这里修改
        pageType: 1, // 1骨架屏,2其他页面(全局模板),3数据渲染成功(正式页面),
        showLogin: false, // 显示登录提示(全局模板),非必须登录的时候可以点击取消修改
        userInfo: {}, // userInfo出来之后才能是3渲染页面

        // * 页面内修改,这里禁止修改,修改之后执行 showpage
        forcedLogin: false,
        // * 页面修改,可随时修改,与页面共存
        showLoading: true, // 显示loading(全局模板)

        // * 其他数据,页面内修改
        loginBeforPageDo: false, // 登录之后是否跳转之前的页面
        loginBeforPage: "", // 登录之后跳转的地址

        // * 审核模式, 只允许审核的时候变更
        isExa: false,
        loginState: '', // ing/end
    },
    mutations: {
        /**
         * 用户账户密码登录成功设置用户数据
         */
        login(state, userInfo) {
            state.hasLogin = true;
            state.userInfo = userInfo;
            // * 用户状态变更
            uni.$emit("userstate", { hasLogin: true });
        },
        /**
         * 用户登出
         */
        logout(state) {
            state.hasLogin = false;
            state.userInfo = {};
            uni.setStorageSync("token", "");
            // * 用户状态变更
            uni.$emit("userstate", { hasLogin: false });
        },
        /**
         * 更新页面展示状态
         */
        showpage(state) {
            if (state.isExa == true) {
                state.pageType = 2; // 审核页面
                state.showLoading = false; // 不显示loading
                return
            }
            if (state.hasLogin == false) {
                // 未登录
                state.showLoading = false; // 不显示loading
                if (state.forcedLogin == true) {
                    state.pageType = 1; // 默认未登录显示页面
                    // 登录状态过去了再显示
                    if (state.loginState == 'ing') {
                        state.showLoading = true;
                        state.showLogin = false; // 默认未登录的显示登录提示
                    } else {
                        state.showLoading = false;
                        state.showLogin = true; // 默认未登录的显示登录提示
                    }
                } else {
                    state.pageType = 3; // 不登录也可显示
                }
                state.loginBeforPageDo = true; // 默认登录回跳
                state.loginBeforPage = "/pages/user/user"; // 默认登录回跳的页面
            } else {
                // 已经登录
                state.showLogin = false; // 不显示登录
                state.showLoading = false; // 不显示loading
                state.pageType = 3;
                // loding 需要显示自行控制
            }
        },
    }
})

export default store