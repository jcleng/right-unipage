# login

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### 说明

> 页面实现

```html
<template>
  <view class="content">
    <view v-if="pageType == 1">
      <text></text>
    </view>
    <view v-if="pageType == 2">
      <com-common></com-common>
    </view>
    <view v-if="pageType == 3">
      <text>index {{userInfo.userid}}</text>
    </view>
    <view v-if="showLoading">
      <com-loading></com-loading>
    </view>
    <view v-if="showLogin">
      <com-login></com-login>
    </view>
  </view>
</template>
```

> 注意: 为实现多端兼容,请不要在全局模板里面使用uni关于ui的api,如showloading/showModal等

> 每一个页面需要实现 onLoad->on userstate 用户状态变换的处理, 这样更完美

```js
onLoad(option) {
    // * 参数直接给到data,方便使用
    this.option = option;
    // * 初始化页面数据
    this.initPage();
    // * 初始化页面显示数据
    this.loadPage();
    // * 需要用户登录的页面需要实现这个方法监听
    uni.$on("userstate", data => {
      // console.log("user userstate");
      // * 用户登录之后重载页面数据
      this.initPage();
      this.loadPage(); // 登录之后页面数据会发生变化
    });
  },
  onUnload() {
    uni.$off("userstate");
  },
```
> 所以的跳转都应当使用js统一实现,不要使用页面跳转

> 登录中的数据,不要显示 showlogin
